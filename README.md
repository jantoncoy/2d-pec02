# 2d Pec01

Juego basado en Mario Bros Nes Nivel 1-1 para el PEC02.

## Como jugar

### Para este juego tiene que utilizar cuatro botones:

- A -> Ir hacia la izquierda
- D -> Ir hacia la derecha
- W -> Saltar

### Pasos para comenzar el juego:

1. Iniciar el ejecutable del juego.

2. Cuando se inicie el juego estara en la escena Menu donde tiene que pulsar encima del texto para empezar a jugar.

3. Debera pasarse el nivel y llegar al castillo del final para salir victorioso.

4. Si muere puede presionar para volver a empezar el nivel.

## Video

[Enlace al video](https://youtu.be/HoCwZUjgQnc "Enlace al video")

## Instaladores

- [ ] [Windows 10](https://drive.google.com/file/d/1SpqTlWEBoSALpKgg9eH8zUdl_R3S9xhl/view?usp=sharing)
- [ ] [Mac OS](https://drive.google.com/drive/folders/1a6t9WVPIX__ATbC26EDI9YmC3yaJXQjK?usp=sharing)
- [ ] [WebGL - Web](http://superninjabros.crzycats.com)

## Partes importantes del codigo

#### Estructura del proyecto

- Assets
 - Scenes: En esta carpeta se encuentra el la scena del juego.
 - Scripts: Se encuentra los scripts de todo el juego.
   - Juego: Los scripts del nivel. (jugador, enemigos, bloques, etc...)
     - Controladores: scripts para los controladores del juego.
	 - Ninja: scripts del protagonista.
	 - Dinosaurio: scripts del enemigo.
	 - Estados: estados que puede tomar el enemigo.
   - Menu: Scripts para los menus del juego.
 - Tiles: Contiene todas las imagenes y tiles del juego.
   - Personajes: Contiene los tiles del protagonista y de los enemigos.
   - Escenario
     - Nivel1: tiles del primer juego.
	   - Prefabs: prefabs utilizados para el nivel 1.
	 - Paletas: contiene las paletas creadas para los tilemaps.
 - Sonidos: Contiene los sonidos del juego.
   - Juego: Tiene los sonidos del juego.
   - Menus: Tiene los sonidos del menu.

#### Equivalencia de elementos 

En este apartado les mostramos las equivalencias entre los elementos de super mario bros y super ninja bros.

**Mapa recreado**

![](https://i.ibb.co/m6CmHm8/mario-1-1.jpg)

**Bloques ladrillo**

![](https://i.ibb.co/Kq4jP6w/14.png)

**Bloque ?**

![](https://i.ibb.co/xSgRRCN/Crate.png)

**Mario**

![](https://i.ibb.co/jwC66gm/Run-001.png)

**Goombas**

![](https://i.ibb.co/s2M4Y2m/Idle-1.png)

**Super Mushrooms**

![](https://i.ibb.co/10KwsGg/Mushroom-1.png)


#### Protagonista

**Archivo: Assets/Scripts/Juego/Ninja/Controlador**

Primero leemos los inputs con la libreria de Inputs de unity.

```csharp
    private void leerInputs()
    {
        bool pulsa = false;

        if (Input.GetKey(KeyCode.Space))
        {
            Debug.Log("Entro Space");
            disparar();
            pulsa = true;
        }
        if (Input.GetKey(KeyCode.W))
        {
            Debug.Log("Entro W");
            saltar();
            pulsa = true;
        }
        if (Input.GetKey(KeyCode.D))
        {
            Debug.Log("Entro D");
            irDerecha();
            pulsa = true;
        }
        if (Input.GetKey(KeyCode.A))
        {
            Debug.Log("Entro A");
            irIzquierda();
            pulsa = true;
        }

        if (!pulsa)
        {
            Debug.Log("No pulso");
            quieto();
        }
    }
```

Una vez que leemos el input detectamos que accion leer, si se mueve o salta se utiliza la velocidad para darle movimiento constante:

```csharp
    private void irDerecha()
    {
        if (miraHaciaIzquierda)
        {
            this.transform.eulerAngles = new Vector3(
                this.transform.eulerAngles.x,
                this.transform.eulerAngles.y + 180,
                this.transform.eulerAngles.z
            );
            miraHaciaIzquierda = false;
        }

        rigidBody.velocity = new Vector2(3,rigidBody.velocity.y);

        //Vector2 fuerza = this.transform.right * 30;
        //rigidBody.AddForce(fuerza);

        animator.SetInteger("Estado",2);
    }
```

Detectamos las colisiones con su funcion de deteccion:

```csharp
    private void irDerecha()
    {
        if (miraHaciaIzquierda)
        {
            this.transform.eulerAngles = new Vector3(
                this.transform.eulerAngles.x,
                this.transform.eulerAngles.y + 180,
                this.transform.eulerAngles.z
            );
            miraHaciaIzquierda = false;
        }

        rigidBody.velocity = new Vector2(3,rigidBody.velocity.y);

        //Vector2 fuerza = this.transform.right * 30;
        //rigidBody.AddForce(fuerza);

        animator.SetInteger("Estado",2);
    }
```

#### Enemigo

**Archivo: Assets/Scripts/Juego/Dinosaurio/Dino**

Utilizamos el controlador para realizar el movimiento y la coordinacion con su cuerpo, al igual que en el protagonista utilizamos la velocidad. El estado contiene todos los estados de IA que se pueden tener en este caso solo andar. 

```csharp
    // Update is called once per frame
    void Update()
    {
        controladorEstado.procesarEstado();
    }
```

Detectamos si alguno de los estados toca el limite o es debilitado por los enemigos:

```csharp
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name == "LimiteDerecho"
        || collision.collider.name == "Muerte"
        || collision.otherCollider.name == "Muerte")
        {
            eliminar();
        }

        if (collision.contacts.Length > 0
            && collision.GetContact(0).point.y > this.transform.position.y)
        {

            if (collision.collider.name.ToLower().Contains("mario"))
            {
                //Golpe desde abajo
                this.GetComponent<Animator>().SetInteger("Estado", 2);
                EventosSonido.aplastar();
                EventosMarcadores.sumarPuntuacion(this.transform.position, 1000);
            }
        }
        controladorEstado.colisiona(collision);
    }
```

**Archivo: Assets/Scripts/Juego/Estados/ControladorEstado.cs**

Dentro del controlador lo limitamos a diez veces por segundo para su ejecucion:

```csharp
public class ControladorEstado
{
    Estado estado;
    Estado andar;
    public GameObject my;
    public Collision2D collision;
    float tiempoMaximo = 100.0f;
    float tiempoActual = 0.000f;

    public ControladorEstado()
    {
        andar = new Andar();
        andar.definirControlador(this);
        estado = andar;
    }

    public void procesarEstado(){
        tiempoActual += Time.deltaTime/0.001f;
        if(tiempoActual >= tiempoMaximo)
        {
            estado.procesarEstado();
            tiempoActual = 0.000f;
        }

    }
}
```
**Archivo: Assets/Scripts/Juego/Estados/Andar.cs**

En el estado andar tenemos la deteccion de la colision si existiera. De esta forma sabemos si tenemos que cambiar de direccion:

```csharp
    /// <summary>
    /// Procesa el estado andar
    /// </summary>
    public void procesarEstado()
    {
        int distanciaActual = ((int)controlador.my.transform.position.x) - distanciaInicial;

        //Cambiamos de direccion si se choca con una pared o otro enemigo
        if(controlador != null
            && controlador.collision != null
            && controlador.collision.otherCollider != null
            && (controlador.collision.otherCollider.name.ToLower().IndexOf("dino") > -1
                || controlador.collision.otherCollider.name.ToLower().IndexOf("pared") > -1
                || controlador.collision.otherCollider.name.ToLower().IndexOf("mario") > -1))
        {
 
            cambiarDireccion();
            //Limpiamos la colision para procesar la siguiente
            controlador.collision = null;
        }
        else
        {
            //Cambiamos direccion si el recorrido maxima a sido superado o igualado
            if (distanciaMaxima <= positivo(distanciaActual))
            {
                cambiarDireccion();
            }
        }

        //Procesamos el movimiento
        Rigidbody2D rigidBody = controlador.my.GetComponent<Rigidbody2D>();
        Animator animator = controlador.my.GetComponent<Animator>();

        if (irDerecha)
        {
            rigidBody.velocity = new Vector2(3, rigidBody.velocity.y);
            
        }
        else
        {
            rigidBody.velocity = new Vector2(-3, rigidBody.velocity.y);
        }

        //cambiamos estado solo si esta quieto
        if(animator.GetInteger("Estado") == 0)
        {
            animator.SetInteger("Estado", 1);
        }
    }
```

#### Bloques

**Archivo: Assets/Scripts/Juego/Golpe.cs**

En los bloques o cajas detectamos el golpe y sus animaciones:

```csharp
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name == "LimiteDerecho"
        || collision.collider.name == "Muerte"
        || collision.otherCollider.name == "Muerte")
        {
            eliminar();
        }

        if (collision.contacts.Length > 0
            && collision.GetContact(0).point.y > this.transform.position.y)
        {

            if (collision.collider.name.ToLower().Contains("mario"))
            {
                //Golpe desde abajo
                this.GetComponent<Animator>().SetInteger("Estado", 2);
                EventosSonido.aplastar();
                EventosMarcadores.sumarPuntuacion(this.transform.position, 1000);
            }
        }
        controladorEstado.colisiona(collision);
    }
```

## Creditos

Creditos de las diferentes obras que se han utilizado:

### Sonidos

- Menu Music 
Autor: **Mrpoly**
Enlace: OpenGameArts

- Happy Arcade Tune
Autor: **Rezoner**
Enlace: OpenGameArts

- 512 Sound Effects (8-bit style)
Autor: **SubspaceAudio**
Enlace: OpenGameArts

### Tiles, Imagenes

- Escenario y Enemigos
Autor: **GameArt2D** - **Zuhria Alfitra**
Enlace: https://www.gameart2d.com/

- Animated Coins
Autor: **Clint Bellanger**
Enlace: OpenGameArts

- Dark Halloween Castle
Autor: **3dproartist786**
Enlace: OpenGameArts