using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Champin : MonoBehaviour
{
    private Rigidbody2D rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rigidbody.velocity = new Vector2(2, rigidbody.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        comprobarColision(collision);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        comprobarColision(collision);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        comprobarColision(collision);
    }

    public void comprobarColision(Collision2D collision)
    {
        //Da contra el jugador
        if (collision.collider.name == "Mario" ||
            collision.collider.name == "Muerte")
        {
            eliminar();
        }
    }

    public void eliminar()
    {
        Destroy(this.gameObject);
    }
}
