using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dino : MonoBehaviour
{
    ControladorEstado controladorEstado;

    // Start is called before the first frame update
    void Start()
    {
        controladorEstado = new ControladorEstado();
        controladorEstado.definirGameObject(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        controladorEstado.procesarEstado();
    }

    private void comprobarCollision(Collision2D collision)
    {
        if (collision.contacts.Length > 0
            && collision.GetContact(0).point.y > this.transform.position.y)
        {
            if (collision.collider.name == "LimiteDerecho"
                || collision.collider.name == "Muerte"
                || collision.otherCollider.name == "Muerte")
            {
                eliminar();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name == "LimiteDerecho"
        || collision.collider.name == "Muerte"
        || collision.otherCollider.name == "Muerte")
        {
            eliminar();
        }

        if (collision.contacts.Length > 0
            && collision.GetContact(0).point.y > this.transform.position.y)
        {

            if (collision.collider.name.ToLower().Contains("mario"))
            {
                //Golpe desde abajo
                this.GetComponent<Animator>().SetInteger("Estado", 2);
                EventosSonido.aplastar();
                EventosMarcadores.sumarPuntuacion(this.transform.position, 1000);
            }
        }
        controladorEstado.colisiona(collision);
    }

    public void eliminar()
    {
        Destroy(this.gameObject);
    }
}
