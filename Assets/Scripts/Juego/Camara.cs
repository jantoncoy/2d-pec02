using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camara : MonoBehaviour
{

    public Transform personaje;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float xCamara = this.transform.position.x;
        float xPersonaje = personaje.position.x;
        if(xPersonaje > xCamara)
        {
            float yCamara = this.transform.position.y;
            float zCamara = this.transform.position.z;
            Vector3 dondeIr = new Vector3(xPersonaje, yCamara, zCamara);
            this.transform.position = dondeIr;
        }
    }
}
