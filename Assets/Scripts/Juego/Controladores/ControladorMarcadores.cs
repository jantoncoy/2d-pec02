using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class EventosMarcadores
{
    public static Action<Vector3, int> sumarPuntuacion;
    public static Action sumarMoneda;
    public static Action empezarJuego;
    public static Action perderJuego;
    public static Action ganarJuego;
}

public class ControladorMarcadores : MonoBehaviour
{
    int puntos;
    int monedas;
    float tiempo;
    bool comenzoJuego;

    public UnityEngine.UI.Text puntosUI;
    public UnityEngine.UI.Text monedasUI;
    public UnityEngine.UI.Text tiempoUI;

    public GameObject indicadores;
    public GameObject menu;
    public GameObject gameOver;
    public GameObject textoPuntuar;
    public GameObject victoria;

    // Start is called before the first frame update
    void Start()
    {
        limpiar();
    }

    public void sumarMoneda()
    {
        monedas += 1;
    }

    public void empezarJuego()
    {
        comenzar();
    }

    public void sumarPuntuacion(Vector3 posicion, int cantidad)
    {
       
        puntos += cantidad;
        Vector3 nuevaPosicion = new Vector3((posicion.x + 2.8f), posicion.y, posicion.z);
        GameObject devolucion = Instantiate(textoPuntuar,nuevaPosicion,textoPuntuar.transform.rotation,indicadores.transform);
        devolucion.GetComponent<Text>().text = cantidad.ToString();
    }

    void comenzar()
    {
        puntos = 0;
        monedas = 0;
        tiempo = 400.0f;
        comenzoJuego = true;
    }

    void limpiar()
    {
        puntos = 0;
        monedas = 0;
        tiempo = 0;
        comenzoJuego = false;
    }

    // Update is called once per frame
    void Update()
    {
        EventosMarcadores.sumarPuntuacion = sumarPuntuacion;
        EventosMarcadores.sumarMoneda = sumarMoneda;
        EventosMarcadores.empezarJuego = empezarJuego;
        EventosMarcadores.perderJuego = perderJuego;
        EventosMarcadores.ganarJuego = ganarJuego;

        puntosUI.text = devolverPuntos();
        monedasUI.text = devolverMonedas();
        if (comenzoJuego)
        {
            procesarTiempo();
        }
        tiempoUI.text = devolverTiempo();
    }

    private void procesarTiempo()
    {
        tiempo -= Time.deltaTime;
    }

    private string devolverPuntos()
    {
        String formato = "{0:000000}";
        String text = string.Format(formato,puntos);
        return text;
    }

    private string devolverMonedas()
    {
        String formato = "{0:00}";
        String text = string.Format(formato, monedas);
        return text;
    }

    private string devolverTiempo()
    {
        String formato = "{0:000}";
        String text = string.Format(formato, tiempo);
        return text;
    }

    public void perderJuego()
    {
        EventosSonido.escenario(false);
        EventosSonido.debilitarse();
        indicadores.SetActive(false);
        menu.SetActive(false);
        gameOver.SetActive(true);
        victoria.SetActive(false);
    }

    public void ganarJuego()
    {
        indicadores.SetActive(false);
        menu.SetActive(false);
        gameOver.SetActive(false);
        victoria.SetActive(true);
    }
}
