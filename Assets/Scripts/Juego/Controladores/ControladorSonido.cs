using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class EventosSonido
{
    //sonidos juego
    public static Action<bool> escenario;
    public static Action salto;
    public static Action lanzar;
    public static Action moneda;
    public static Action debilitarse;
    public static Action aplastar;

    //sonidos menu
    public static Action<bool> ambiente;
    public static Action pulsar;
}


public class ControladorSonido : MonoBehaviour
{
    //sonidos juego
    public AudioSource escenario;
    public AudioSource salto;
    public AudioSource lanzar;
    public AudioSource moneda;
    public AudioSource debilitarse;
    public AudioSource aplastar;

    //sonidos menu
    public AudioSource ambiente;
    public AudioSource pulsar;

    public void Start()
    {
        EventosSonido.escenario += activarEscenario;
        EventosSonido.salto += activarSalto;
        EventosSonido.lanzar += activarLanzar;
        EventosSonido.moneda += activarMoneda;
        EventosSonido.debilitarse += activarDebilitarse;
        EventosSonido.aplastar += activarAplastar;

        EventosSonido.ambiente += activarAmbiente;
        EventosSonido.pulsar += activarPulsar;

        inicializarAudioSource();
    }

    private void inicializarAudioSource()
    {
        escenario = GameObject.Find("Escenario").GetComponent<AudioSource>();
        salto = GameObject.Find("Salto").GetComponent<AudioSource>();
        aplastar = GameObject.Find("Aplastar").GetComponent<AudioSource>();
        ambiente = GameObject.Find("MenuSonido").GetComponent<AudioSource>();
        pulsar = GameObject.Find("Pulsar").GetComponent<AudioSource>();
        moneda = GameObject.Find("Moneda").GetComponent<AudioSource>();
        lanzar = GameObject.Find("Lanzar").GetComponent<AudioSource>();
        debilitarse = GameObject.Find("Debilitarse").GetComponent<AudioSource>();
    }

    public void activarEscenario(bool activar)
    {
        comprobarAudios(escenario);
        if (activar)
            escenario.Play();
        else
            escenario.Stop();
    }

    private void comprobarAudios(AudioSource entrada)
    {
        if(entrada == null)
        {
            inicializarAudioSource();
        }
    }

    public void activarSalto()
    {
        comprobarAudios(salto);
        salto.Play();
    }

    public void activarLanzar()
    {
        comprobarAudios(lanzar);
        lanzar.Play();
    }

    public void activarMoneda()
    {
        comprobarAudios(moneda);
        moneda.Play();
    }

    public void activarDebilitarse()
    {
        comprobarAudios(debilitarse);
        debilitarse.Play();
    }

    public void activarAplastar()
    {
        comprobarAudios(aplastar);
        aplastar.Play();
    }

    public void activarAmbiente(bool activar)
    {
        comprobarAudios(ambiente);
        if (activar)
            ambiente.Play();
        else
            ambiente.Stop();
    }

    public void activarPulsar()
    {
        comprobarAudios(pulsar);
        pulsar.Play();
    }
}
