using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorEstado
{
    Estado estado;
    Estado andar;
    public GameObject my;
    public Collision2D collision;
    float tiempoMaximo = 100.0f;
    float tiempoActual = 0.000f;

    public ControladorEstado()
    {
        andar = new Andar();
        andar.definirControlador(this);
        estado = andar;
    }

    public void procesarEstado(){
        tiempoActual += Time.deltaTime/0.001f;
        if(tiempoActual >= tiempoMaximo)
        {
            estado.procesarEstado();
            tiempoActual = 0.000f;
        }

    }

    /// <summary>
    /// Estado que recoge la colision para poder procesarla en el siguiente update
    /// </summary>
    /// <param name="my"></param>
    /// <param name="collision"></param>
    public void colisiona(Collision2D collision)
    {
        this.collision = collision;
    }

    /// <summary>
    /// Definimos el gameobject
    /// </summary>
    /// <param name="gameobject"></param>
    public void definirGameObject(GameObject gameobject)
    {
        this.my = gameobject;
    }
}
