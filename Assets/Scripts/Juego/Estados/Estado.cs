using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Estado
{
    public void definirControlador(ControladorEstado controlador);
    public void procesarEstado();
}
