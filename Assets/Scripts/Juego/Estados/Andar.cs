using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Andar : Estado
{
    ControladorEstado controlador;
    int distanciaMaxima = 4;
    int distanciaInicial = 0;

    bool irDerecha;

    /// <summary>
    /// Constructor
    /// </summary>
    public Andar()
    {
        irDerecha = true;
    }

    /// <summary>
    /// Define su controlador
    /// </summary>
    /// <param name="controlador"></param>
    public void definirControlador(ControladorEstado controlador)
    {
        this.controlador = controlador;
    }

    /// <summary>
    /// Procesa el estado andar
    /// </summary>
    public void procesarEstado()
    {
        int distanciaActual = ((int)controlador.my.transform.position.x) - distanciaInicial;

        //Cambiamos de direccion si se choca con una pared o otro enemigo
        if(controlador != null
            && controlador.collision != null
            && controlador.collision.otherCollider != null
            && (controlador.collision.otherCollider.name.ToLower().IndexOf("dino") > -1
                || controlador.collision.otherCollider.name.ToLower().IndexOf("pared") > -1
                || controlador.collision.otherCollider.name.ToLower().IndexOf("mario") > -1))
        {
 
            cambiarDireccion();
            //Limpiamos la colision para procesar la siguiente
            controlador.collision = null;
        }
        else
        {
            //Cambiamos direccion si el recorrido maxima a sido superado o igualado
            if (distanciaMaxima <= positivo(distanciaActual))
            {
                cambiarDireccion();
            }
        }



        //Procesamos el movimiento
        Rigidbody2D rigidBody = controlador.my.GetComponent<Rigidbody2D>();
        Animator animator = controlador.my.GetComponent<Animator>();

        if (irDerecha)
        {
            rigidBody.velocity = new Vector2(3, rigidBody.velocity.y);
            
        }
        else
        {
            rigidBody.velocity = new Vector2(-3, rigidBody.velocity.y);
        }

        //cambiamos estado solo si esta quieto
        if(animator.GetInteger("Estado") == 0)
        {
            animator.SetInteger("Estado", 1);
        }
    }

    /// <summary>
    /// Cambia la direccion de la entidad
    /// </summary>
    private void cambiarDireccion()
    {
        distanciaInicial = (int)controlador.my.transform.position.x;
        if (irDerecha)
        {
            irDerecha = false;
        }
        else
        {
            irDerecha = true;
        }

        girarCuerpo();
    }

    /// <summary>
    /// Gira la entidad 
    /// </summary>
    private void girarCuerpo()
    {
        controlador.my.transform.eulerAngles = new Vector3(
            controlador.my.transform.eulerAngles.x,
            controlador.my.transform.eulerAngles.y + 180,
            controlador.my.transform.eulerAngles.z
        );
    }

    private int positivo(int valor)
    {
        if(valor < 0)
        {
            return (valor * -1);
        }
        return valor;
    }
}
