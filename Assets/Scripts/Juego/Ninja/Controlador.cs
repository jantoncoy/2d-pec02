using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlador : MonoBehaviour
{

    Animator animator;
    Efectos efectos;
    Rigidbody2D rigidBody;
    bool miraHaciaIzquierda = false;

    int [] estadoActuales;
    int deteccionInputs = 1000/20;//detectaremos
    float acumulado = 0;

    // Start is called before the first frame update
    void Start()
    {
        animator = this.GetComponent<Animator>();
        efectos = new Efectos(this.gameObject);
        rigidBody = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Comenzar.iniciarJuego)
        {
            if (acumulado > deteccionInputs)
            {
                leerInputs();
                acumulado = 0;
            }
            else
            {
                acumulado += Time.deltaTime / 0.001f;
            }
        }
    }

    private void irDerecha()
    {
        if (miraHaciaIzquierda)
        {
            this.transform.eulerAngles = new Vector3(
                this.transform.eulerAngles.x,
                this.transform.eulerAngles.y + 180,
                this.transform.eulerAngles.z
            );
            miraHaciaIzquierda = false;
        }

        rigidBody.velocity = new Vector2(3,rigidBody.velocity.y);

        //Vector2 fuerza = this.transform.right * 30;
        //rigidBody.AddForce(fuerza);

        animator.SetInteger("Estado",2);
    }

    private void irIzquierda()
    {
        if (!miraHaciaIzquierda)
        {
            this.transform.eulerAngles = new Vector3(
                this.transform.eulerAngles.x,
                this.transform.eulerAngles.y + 180,
                this.transform.eulerAngles.z
            );
            miraHaciaIzquierda = true;
        }

        rigidBody.velocity = new Vector2(-3, rigidBody.velocity.y);
        //Vector2 fuerza = this.transform.right * 30;
        //rigidBody.AddForce(fuerza);

        animator.SetInteger("Estado", 2);
    }

    private void saltar()
    {
        if(rigidBody.velocity.y == 0){
            //animator.SetInteger("Estado", 1);
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, 9.5f);
            EventosSonido.salto();
        }

    }

    private void disparar()
    {
        if (Efectos.powerUpFuego)
        {
            animator.SetInteger("Estado", 3);
        }
    }

    private void quieto()
    {
        rigidBody.velocity = new Vector2(0, rigidBody.velocity.y);
        animator.SetInteger("Estado", 0);
    }

    private void leerInputs()
    {
        bool pulsa = false;

        if (Input.GetKey(KeyCode.Space))
        {
            Debug.Log("Entro Space");
            disparar();
            pulsa = true;
        }
        if (Input.GetKey(KeyCode.W))
        {
            Debug.Log("Entro W");
            saltar();
            pulsa = true;
        }
        if (Input.GetKey(KeyCode.D))
        {
            Debug.Log("Entro D");
            irDerecha();
            pulsa = true;
        }
        if (Input.GetKey(KeyCode.A))
        {
            Debug.Log("Entro A");
            irIzquierda();
            pulsa = true;
        }

        if (!pulsa)
        {
            Debug.Log("No pulso");
            quieto();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        comprobarColision(collision);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        comprobarColision(collision);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        comprobarColision(collision);
    }

    private void comprobarColision(Collision2D collision)
    {
        if (collision.collider.name == "muerte") // Tocas un dino(goomba)
        {
            if (!Efectos.agrandado)
                animator.SetInteger("Estado", 4);
            else
            {
                Efectos.agrandado = false;
                this.transform.localScale = new Vector3(0.3446948f, 0.3446948f, 0.3446948f);
            }
        }
        else if (collision.collider.name == "Muerte")//te cuelas por un agujero
        {
            animator.SetInteger("Estado", 4);
        }
        else if (collision.collider.name.ToLower().Contains("champi"))//coges un champiñon
        {
            EventosMarcadores.sumarPuntuacion(this.transform.position, 2000);
            agrandar();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Castillo")
        {
            ganar();
        }
    }

    public void perder()
    {
        //desactivamos controles
        Comenzar.iniciarJuego = false;

        //Mostramos mensaje de game over
        EventosMarcadores.perderJuego();

    }

    public void agrandar()
    {
        Efectos.agrandado = true;
        this.transform.localScale += new Vector3(0.6f, 0.6f, 0.6f);
        animator.SetInteger("Estado", 5);
    }

    public void ganar()
    {
        EventosMarcadores.ganarJuego();
    }
}
