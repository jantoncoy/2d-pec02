using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Efectos
{

    public static bool agrandado = false;
    public static bool powerUpFuego = false;
    GameObject gameObject = null;

    public Efectos(GameObject agente)
    {

    }

    /// <summary>
    /// Se encarga de dejar el estado de agrandado
    /// </summary>
    void agrandar()
    {
        agrandado = true;
        
    }

    /// <summary>
    /// Deja al personaje con sus dimnensiones originales
    /// </summary>
    void minimizar()
    {
        agrandado = false;

    }

    /// <summary>
    /// Se encarga de dejar en modo enrojecido al personaje
    /// </summary>
    void enrojecer()
    {
        powerUpFuego = true;

    }

    /// <summary>
    /// Vuelve a dejar al personaje con su color original
    /// </summary>
    void normalizar()
    {
        powerUpFuego = false;

    }
}
