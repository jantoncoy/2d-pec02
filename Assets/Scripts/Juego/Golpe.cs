using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Golpe : MonoBehaviour
{
    public bool esPowerUp = false;
    public bool esAgrandar = false;
    public bool esBloque = true;
    public bool esMoneda = false;

    public GameObject powerUP;
    public GameObject agrandar;
    public GameObject moneda;

    public int puntuacion = 100;

    private Vector3 posicionOriginal;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.GetContact(0).point.y < this.transform.position.y)
        {
            //Golpe desde abajo
            this.GetComponent<Animator>().SetBool("Golpeado", true);
        }
    }

    public void golpear()
    {
        posicionOriginal = this.transform.position;
        this.transform.Translate(0, 0.2f, 0);
        if(puntuacion != 0)
        {
            if (!esBloque)
            {
                SpriteRenderer renderer = GetComponent<SpriteRenderer>();
                Vector3 posicionNuevoObjeto = new Vector3(posicionOriginal.x, posicionOriginal.y + 1f, posicionOriginal.z);
                if (esPowerUp)
                {
                    Instantiate(powerUP, posicionNuevoObjeto, this.transform.rotation);
                }
                else if (esAgrandar)
                {
                    Instantiate(agrandar, posicionNuevoObjeto, this.transform.rotation);
                }
                renderer.color = new Color(0.7f, 0.7f, 0.7f);
                EventosMarcadores.sumarPuntuacion(this.transform.position, puntuacion);
                puntuacion = 0;
            }
            else if(Efectos.agrandado)
            {
                EventosMarcadores.sumarPuntuacion(this.transform.position, 50);
                this.GetComponent<Animator>().SetBool("Destruido", true);
                EventosMarcadores.sumarPuntuacion(this.transform.position, puntuacion);
            }

        }
    }

    public void dejarGolpear()
    {
        this.transform.position = posicionOriginal;
        this.GetComponent<Animator>().SetBool("Golpeado", false);
    }

    public void eliminar()
    {
        Destroy(this.gameObject);
    }
}
