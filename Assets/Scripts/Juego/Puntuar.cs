using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puntuar : MonoBehaviour
{
    float tiempoDeVida;

    // Start is called before the first frame update
    void Start()
    {
        tiempoDeVida = 0;
    }

    // Update is called once per frame
    void Update()
    {
        tiempoDeVida += Time.deltaTime;
        this.transform.Translate(0, 0.06f, 0);
        if(tiempoDeVida > 4)
        {
            eliminar();
        }
    }

    private void eliminar()
    {
        Destroy(this.gameObject);
    }
}
