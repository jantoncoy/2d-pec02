using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Comenzar : MonoBehaviour
{
    public GameObject menu;
    static public bool iniciarJuego;
    static public bool activoSonidoAmbiente;

    // Start is called before the first frame update
    void Start()
    {
        iniciarJuego = false;
    }

    private void Update()
    {
        if (!activoSonidoAmbiente)
        {
            EventosSonido.ambiente(true);
            activoSonidoAmbiente = true;
        }
        
    }

    public void inicio()
    {
        menu.SetActive(false);
        iniciarJuego = true;
        EventosSonido.ambiente(false);
        EventosSonido.escenario(true);
        EventosMarcadores.empezarJuego();
    }

    public void reiniciar()
    {
        SceneManager.LoadScene("Juego");
    }
}
